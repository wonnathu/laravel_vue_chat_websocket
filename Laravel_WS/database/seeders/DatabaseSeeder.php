<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Table;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $user = new User();
        $user->name = "admin";
        $user->email = "admin@admin.com";
        $user->password = bcrypt("123456");
        $user->save();

        $user = new User();
        $user->name = "user";
        $user->email = "user@user.com";
        $user->password = bcrypt("123456");
        $user->save();

        \App\Models\Table::factory(5)->create();
    }
}
