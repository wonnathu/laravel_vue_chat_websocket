<?php

use App\Events\Hello;
use Illuminate\Support\Facades\Route;

Route::get('/broadcast', function () {
    broadcast(new Hello("here", "there"));
});

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia\Inertia::render('Dashboard');
})->name('dashboard');
