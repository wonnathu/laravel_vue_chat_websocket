<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\MessageController;
use App\Http\Controllers\API\TableController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//Message
Route::middleware('auth:sanctum')->post('/message', [MessageController::class, 'store']);

//get Rooms
Route::middleware('auth:sanctum')->get('/tables', [TableController::class, 'getTables']);

Route::post('/login', [LoginController::class, 'login']);
Route::get('/test', function () {
    return 'here';
});
Route::middleware('auth:sanctum')->post('/logout', [LoginController::class, 'logout']);
