<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = User::where('email', '=', $request->email)->first();
        if (Hash::check($request->password, $user->password)) {
            $token = $user->createToken('token-name');
            return $token->plainTextToken;
        }
        return false;
    }

    public function logout(Request $request)
    {
        $token = $request->user()->currentAccessToken()->delete();
        return $token;
    }
}
